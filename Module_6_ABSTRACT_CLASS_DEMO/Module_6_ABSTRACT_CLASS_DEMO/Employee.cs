﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6_ABSTRACT_CLASS_DEMO
{
    abstract class Employee
    {
        public virtual void Login()
        {
            Console.WriteLine("Employee Login");
        }
        public abstract void Hire();

       
    }
}
