﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_4_enum
{
    class Program
    {
        //Lun = 1 per evitare che parte da zero
        enum Days { Lun = 1, Mar, Merc, Giov, Ven, Sab, Dom };

        static void Main(string[] args)
        {
            int x = (int)Days.Dom;
            int y = (int)Days.Ven;
            Console.WriteLine("Dom = {0}", x);
            Console.WriteLine("Ven = {0}", y);

            Console.ReadKey();
        }
    }
}
