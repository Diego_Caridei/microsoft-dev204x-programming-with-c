﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_8_Generics
{
    class Program
    {

        #region GenericCollection

        static void CreateGenericList()
        {
            string s1 = "Latte";
            string s2 = "Espresso";
            string s3 = "Americano";
            string s4 = "Capuccino";
            string s5 = "Mocha";
            
            var coffeBeverages = new List<String>();
            coffeBeverages.Add(s1);
            coffeBeverages.Add(s2);
            coffeBeverages.Add(s3);
            coffeBeverages.Add(s4);
            coffeBeverages.Add(s5);

            foreach (String i in coffeBeverages)
            {
                Console.WriteLine(i);
            }

            Coffe coffee1 = new Coffe(4, "Arabica", "Columbia");
            Coffe coffee2 = new Coffe(2, "Arabica", "Vietnam");
            Coffe coffee3 = new Coffe(3, "Robusta", "Indonesia");

            var hotBeverages = new List<Coffe>();
            hotBeverages.Add(coffee1);
            hotBeverages.Add(coffee2);
            hotBeverages.Add(coffee3);

            foreach(Coffe i in hotBeverages){
                Console.WriteLine(i.Bean);
            }
        }
        #endregion

        static void Main(string[] args)
        {
            CreateGenericList();
            Console.ReadKey();
        }
    }
}
