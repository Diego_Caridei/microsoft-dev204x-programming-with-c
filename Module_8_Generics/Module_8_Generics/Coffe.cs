﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_8_Generics
{
    class Coffe
    {
        private string bean;
        private string countryOfOrigin;
        private int strenght;


        public Coffe(int strenght, string bean, string country)
        {
            this.Strenght = strenght;
            this.Bean = bean;
            this.CountryOfOrigin = country;
        }

        public int Strenght
        {
            get { return strenght; }
            set { strenght = value; }
        }

        public string CountryOfOrigin
        {
            get { return countryOfOrigin; }
            set { countryOfOrigin = value; }
        }

        public string Bean
        {
            get { return bean; }
            set { bean = value; }
        }

    }
}
