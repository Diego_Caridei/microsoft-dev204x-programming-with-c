﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_4_Structure
{
    class Program
    {
        struct Coffee
        {
            public string grind;
            public string Name { get; set; }
            public string Bean { get; set; }
            public string CountryOfOrigin { get; set; }
            public int Strenght { get; set; }

        }

        static void Main(string[] args)
        {
            Coffee coffe1 = new Coffee();
            coffe1.Name = "Fourth Coffee Quencher";
            coffe1.CountryOfOrigin = "Indonesia";
            coffe1.Strenght = 3;
            coffe1.grind = "fine";
            Console.WriteLine("Name: {0}", coffe1.Name);
            Console.WriteLine("Country of origin: {0}", coffe1.CountryOfOrigin);
            Console.WriteLine("Strenght: {0}", coffe1.Strenght);
            Console.WriteLine("Grind: {0}", coffe1.grind);

            Console.ReadKey();


        }
    }
}
