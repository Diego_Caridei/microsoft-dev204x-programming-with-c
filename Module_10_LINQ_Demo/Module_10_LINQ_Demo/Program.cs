﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_10_LINQ_Demo
{
    class Program
    {
        private List<Product> productList;
        static void Main(string[] args)
        {
            //Test Linq
            Program prog = new Program();
            prog.Esempio1();
            Console.ReadKey();         
        }

        public void Esempio2()
        {
            List<Product> products = GetProductList();
        }

         public List<Product> GetProductList()
        {
            if (productList == null)
            {

            }
            return productList;
        }
        public void Esempio1()
        {
            int[] numbers = { 5, 4, 1, 4, 9, 6, 7, 2, 0 };
            //Seleziono tutti i numeri minori di 5 contenuti in numbers
            var lowNums = from num in numbers
                          where num < 5
                          select num;
            //Stampo
            Console.WriteLine("Numbers <5:");
            foreach (var i in lowNums)
            {
                Console.WriteLine(i);
            }
        }
    }
}
