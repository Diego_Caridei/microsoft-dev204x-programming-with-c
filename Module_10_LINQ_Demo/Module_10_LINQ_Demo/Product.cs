﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_10_LINQ_Demo
{
    class Product
    {
        public int ProductID { get; set; }
        public string ProductName{ get; set; }
        public string Category { get; set; }
        public decimal UnitPrice { get; set; }
        public int UnitInStock { get; set; }

    }
}
