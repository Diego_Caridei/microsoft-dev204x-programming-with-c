﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Five_Assignment
{
    class Course
    {
      
        public Course(string name, int credits = 0, Student[] students = null, Teacher[] teachers = null)
        {
            this.Name = name;
            this.Credits = credits;
            this.Students = students;
            this.Teachers = teachers;
        }

        public Course()
        {

        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int _credits;

        public int Credits
        {
            get { return _credits; }
            set { _credits = value; }
        }
        private Student[] _students;

        internal Student[] Students
        {
            get { return _students; }
            set { _students = value; }
        }
        private Teacher[] _teachers;

        internal Teacher[] Teachers
        {
            get { return _teachers; }
            set { _teachers = value; }
        }

    }
}
