﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Five_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {

            Student student1 = new Student("Diego", "Caridei", new DateTime(1985, 04, 02), "Via pippo", "Napoli", "Italy", 80100, "Italy");
            Student student2 = new Student("Fabio", "Nisci", new DateTime(1990, 06, 10), "Via nonna", "Milano", "Italy", 8910, "Italy");
            Student student3 = new Student("Salvo", "Camma", new DateTime(1987, 08, 12), "Via palermo", "Torino", "Italy", 2020, "Italy");

            Course edxCourse = new Course("Programming with C#", 4);

            Student[] students = new Student[3];
            students[0] = student1;
            students[1] = student2;
            students[2] = student3;

            edxCourse.Students = students;

            Teacher teacher = new Teacher("Giulio", "Giunta", new DateTime(1957, 01, 01), "Via ganturco", "Napoli", "Italy", 80100, "Italy");
            Teacher[] teachers = new Teacher[1];
            edxCourse.Teachers = teachers;

            Degree degree = new Degree("Bachelor of Science", 200, edxCourse);

            UProgram program = new UProgram("Information Technology", "Giulio Giunta",degree);

            Console.WriteLine("The {0} program contains the {1} degree", program.ProgramName, program.Degree.Name);
            Console.WriteLine("The {0} degree contains the course {1}", program.Degree.Name, program.Degree.Course.Name);
            Console.WriteLine("The {0} course contains {1} students", program.Degree.Course.Name, Student.Total_students);
            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey();


        }
    }
}
