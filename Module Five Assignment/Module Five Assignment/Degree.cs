﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Five_Assignment
{
    class Degree
    {

        public Degree(string name, int credit, Course course)
        {
            this.Name = name;
            this.TotalCredits = credit;
            this.Course = course;

        }

        public Degree()
        {

        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int _totalCredits;

        public int TotalCredits
        {
            get { return _totalCredits; }
            set { _totalCredits = value; }
        }
        private Course _course;

        internal Course Course
        {
            get { return _course; }
            set { _course = value; }
        }

    }
}
