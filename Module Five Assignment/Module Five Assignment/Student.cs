﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Five_Assignment
{
    class Student
    {

        public Student()
        {

        }
        private static int studentsCount = 0;

        public Student(string name, string lastname, DateTime birthDate, string addressLine, string city, string state, int postal, string country)
        {
            this.FirstName = name;
            this.LastName = lastname;
            this.BirthDate = birthDate;
            this.AdddressLine = addressLine;
            this.City = city;
            this.State = state;
            this.Postal = postal;
            this.Country = country;
            total_students++;
        }

        private static int total_students = 0;

        public static int Total_students
        {
            get { return Student.total_students; }
            set { Student.total_students = value; }
        }


       

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        private DateTime _birthDate;

        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }
        private string _adddressLine;

        public string AdddressLine
        {
            get { return _adddressLine; }
            set { _adddressLine = value; }
        }
        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        private string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        private int _postal;

        public int Postal
        {
            get { return _postal; }
            set { _postal = value; }
        }
        private string _country;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }


          
    }
}
