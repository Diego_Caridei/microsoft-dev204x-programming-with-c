﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Five_Assignment
{
    class UProgram
    {


        public UProgram(string name, string departH, Degree degree)
        {
            this.ProgramName = name;
            this.DepartmentHead = departH;
            this.Degree = degree;
        }

        public UProgram()
        {

        }


        private string _programName;

        public string ProgramName
        {
            get { return _programName; }
            set { _programName = value; }
        }
        private string _departmentHead;

        public string DepartmentHead
        {
            get { return _departmentHead; }
            set { _departmentHead = value; }
        }


        private Degree _degree;

        internal Degree Degree
        {
            get { return _degree; }
            set { _degree = value; }
        }
    }
}
