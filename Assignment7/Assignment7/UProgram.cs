﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class UProgram
    {
        public string ProgramName { get; set; }
        public string DepartmentHead { get; set; }
        public Degree Degree { get; set; }

        public UProgram(string name, string departmentH, Degree degree)
        {
            this.ProgramName = name;
            this.DepartmentHead = departmentH;
            this.Degree = degree;
        }
    }
}
