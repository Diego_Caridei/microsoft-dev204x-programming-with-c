﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class Degree
    {
        public string DegreeName { get; set; }
        public int TotalCredits { get; set; }
        public Course Course { get; set; }

        // Constructor.
        public Degree(string dName, int credits, Course course)
        {
            this.DegreeName = dName;
            this.TotalCredits = credits;
            this.Course = course;
        }
    }
}