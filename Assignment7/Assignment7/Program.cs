﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("Diego", "Caridei", new DateTime(1990, 10, 22), "diego@mail.com");
            Student student2 = new Student("Michele", "Sannino", new DateTime(1950, 03, 12), "Mike@mail.com");
            Student student3 = new Student("Sara", "task", new DateTime(1985, 04, 03), "Sara@mail.com");
            ArrayList students = new ArrayList();
            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            foreach (Student s in students)
            {
                for (int i = 0; i < 5; i++)
                {
                    double grade = i + 5.53;
                    s.addGrade(grade);
                }
            }

            Course course = new Course("Programming with C#", 15);

            course.addStudent(student1);
            course.addStudent(student2);
            course.addStudent(student3);

            foreach (Student s in students)
            {
                s.popGrade();
                s.addGrade(10);
            }

            foreach (Student s in students)
            {
                double grade5 = s.popGrade();
                double grade4 = s.popGrade();
                s.popGrade();
                s.addGrade(0);
                s.addGrade(grade4);
                s.addGrade(grade5);
            }

            course.listStudents();
            Console.ReadKey();
        }
    }
}