﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment7
{
    class Course
    {
        private static int maxStudents = 3;
        private static int maxTeachers = 3;

        public string CourseName { get; set; }
        public int CourseCredits { get; set; }
        private ArrayList students;
        private ArrayList teachers;

        public Course(string cName, int credits)
        {
            this.CourseName = cName;
            this.CourseCredits = credits;
            this.students = new ArrayList();
            this.teachers = new ArrayList();
        }

        public void addStudent(Student student)
        {
            int n = this.students.Count;
            if (n < maxStudents)
            {
                this.students.Add(student);
            }
            else
            {
                Console.WriteLine("Array of students is full.");
            }
        }

        public int getStudentsNumber()
        {
            return this.students.Count;
        }

        public void listStudents()
        {
            // Sort the array.
            this.students.Sort();

            foreach (Student s in this.students)
            {
                Console.WriteLine("{0} {1}", s.FirstName, s.LastName);
                s.showGrades();
            }
        }

        public void addTeacher(Teacher teacher)
        {
            int n = this.teachers.Count;
            if (n < maxTeachers)
            {
                this.teachers.Add(teacher);
            }
            else
            {
                Console.WriteLine("Array of teachers is full.");
            }
        }

        public int getTeacherNumber()
        {
            return this.teachers.Count;
        }
    }
}