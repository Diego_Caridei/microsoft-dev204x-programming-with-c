﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Two_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            bool alternate = true;
            for (int i = 0; i < 7; i++)
            {
                for (int index = 0; index < 7; index++)
                {
                    if (alternate == true)
                    {
                        Console.Write("X");
                        alternate = false;
                    }
                    else
                    {
                        Console.Write("O");
                        alternate = true;
                    }

                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}

