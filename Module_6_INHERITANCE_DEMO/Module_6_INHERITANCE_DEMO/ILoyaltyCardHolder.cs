﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6_INHERITANCE_DEMO
{
    interface ILoyaltyCardHolder
    {
       int TotalPoints { get; }
        int AddPoints(decimal transictionValue);
        void ResetPoints();
    }
}
