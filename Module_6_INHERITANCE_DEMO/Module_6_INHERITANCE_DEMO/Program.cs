﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6_INHERITANCE_DEMO
{
    class Program
    {
        static void Main(string[] args)
        {

            
             
            Manager myManager = new Manager();
            myManager.Department = "Capo dipartimento Informatico";
            myManager.FirstName = "Diego";
            myManager.Login();
            myManager.Hire();

            Console.ReadKey();

        }
    }
}
