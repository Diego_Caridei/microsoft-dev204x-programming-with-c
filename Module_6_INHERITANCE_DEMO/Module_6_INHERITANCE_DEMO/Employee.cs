﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6_INHERITANCE_DEMO
{
   abstract class Employee
    {

       public virtual void Login()
       {
           Console.WriteLine("Emoloyee Login");
       }

       public abstract void Hire();

        private string empNumber;
        private string firstName;
        private string lastName;
        private string address;


        public string EmpNumber
        {
            get { return empNumber; }
            set { empNumber = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

    }
}
