﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6_INHERITANCE_DEMO
{
    class Manager : Employee
    {
        private string department;


        public override void Login()
        {
            Console.WriteLine("Manager Login");
        }
        public override void Hire()
        {
            Console.WriteLine("Hire someone");

        }



        public string Department
        {
            get { return department; }
            set { department = value; }
        }

    }
}
