﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_6_INHERITANCE_DEMO
{
    class Customer : ILoyaltyCardHolder
    {


        private int totalPoints;

        public int TotalPoints
        {
            get { return totalPoints; }
        }

        public int AddPoints(decimal transictionValue)
        {
            int points = Decimal.ToInt32(transictionValue);
            totalPoints += points;
            return totalPoints;
        }

        public void ResetPoints()
        {
            totalPoints = 0;
        }



    
    }
}
