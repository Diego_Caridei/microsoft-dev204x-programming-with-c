# Microsoft: DEV204x Programming with C# #

![edx-microsoft-logo.jpg](https://bitbucket.org/repo/9zoaE8/images/1239304987-edx-microsoft-logo.jpg)

#Module 0
Important Pre-Course Survey

About this Course

Introduce Yourself

#Module One
Introducing C#

The Tools

Data Types, Variables, Operators, and Expressions

Hands-On Practice

Module One Assignment

Homework



#Module Two
Making Decisions and Performing Iterations

C# Decision Statements

Repetition in C# 

Hands-On Practice

Module Two Assignment


#Module Three	

Working with Methods and Handling Exceptions

Methods

Exception Handling

Hands-On Practice

Module Three Assignment Instructions

#Module Four
Introducing Complex Data Structures

Working with Arrays 

Working with Enumerations (enum) 

Working with Structures (struct) 

Hands-On Practice

Module Four Assignment Instructions
#Module Five

Introducing Object-Oriented Programming in C#

Classes in C#

Encapsulation

Static Methods and Static Classes

Anonymous Classes

Hands-On Practice

Module Five Assignment Instructions

#Module Six
More OOP

Resource Management

Hands-On Practice

Module Six Assignment Instructions

#Module Seven

C# Collections

Working with Collections

Hands-On Practice

Module Seven Assignment Instructions

#Module Eight
Introducing Generics

Working with Generics

Hands-On Practice

Module Eight Assignment Instructions

#Module Nine

Events and Delegates

Using Events and Delegates

Hands-On Practice

Module Nine Assignment Instructions

#Module Ten
Introducing LINQ

Using LINQ

Hands-On Practice

Module Ten Assignment Instructions

#Module Eleven
Improving Application Performance

Multitasking and Parallel Programming 

More on Tasks

Hands-On Practice

Module Eleven Assignment Instructions

#Module Twelve
Asynchronous Programming

Asynchronous Programming in C#

Hands-On Practice

Module Twelve Assignment Instructions