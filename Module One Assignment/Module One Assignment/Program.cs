﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_One_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            //Student
            string studentFirstName = "Diego";
            string studentLastName = "Caridei";
            string studentBirthdate = "02/04/1985";
            string studentAddressLine1 = "Via pippo";
            string studentCity = "Napoli";
            string studentState = "Italy";
            int studentPostal = 80100;
            string studentCountry = "Italy";

            //Teacher Information

            string teacherFirstName = "Giovanni";
            string teacherLastName = "Mucco";
            string teacherBirthdate = "13/09/1970";
            string teacherAddressLine1 = "Via tarallo";
            string teacherCity = "Milano";
            string teacherState = "Italy";
            int teacherPostal = 2046;
            string teacherCountry = "Italy";


            //UProgram Information
            string programName = "Computer Science";
            string departmentHead = teacherFirstName + " " + teacherLastName;
            int degrees = 100;

            //Course Information
            string courseName = "Programming with C#";
            int credits = 12;
            int durationInWeeks = 12;

            Console.WriteLine("The student name is {0} {1} ", studentFirstName, studentLastName);
            Console.WriteLine("he live in " + studentCity);
            Console.ReadKey();


        }
    }
}
