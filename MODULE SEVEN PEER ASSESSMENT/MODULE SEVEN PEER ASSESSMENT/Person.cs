﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_SEVEN_PEER_ASSESSMENT
{
    class Person
    {
        private string firstName;
        private string lastName;
        private DateTime birthDate;
        private string addressLine;
        private string city;
        private string state;
        private string zip;
        private string country;

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        public string Zip
        {
            get { return zip; }
            set { zip = value; }
        }


        public string State
        {
            get { return state; }
            set { state = value; }
        }


        public string City
        {
            get { return city; }
            set { city = value; }
        }



        public string AddressLine
        {
            get { return addressLine; }
            set { addressLine = value; }
        }


        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }


        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
    }
}
