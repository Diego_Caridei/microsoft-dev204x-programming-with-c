﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_SEVEN_PEER_ASSESSMENT
{
    class Teacher : Person, IComparable
    {
        private static int teachersCount = 0;
        public Teacher(string name, string lastname, DateTime birthDate, string addressLine, string city, string state, string zip, string country)
        {
            this.FirstName = name;
            this.LastName = lastname;
            this.BirthDate = birthDate;
            this.AddressLine = addressLine;
            this.City = city;
            this.State=state;
            this.Zip=zip;
            this.Country = country;
            teachersCount++;

        }
        public void GradeTest()
        {
            Console.WriteLine("The teacher {0} is grading the test.", this.FirstName + " " + this.LastName);
        }

        public int TeachersInSchool()
        {
            return teachersCount;
        }

        public int CompareTo(object obj)
        {
            Teacher teacherC = (Teacher)obj;
            string nameC = teacherC.FirstName + " " + teacherC.LastName;
            string name = this.FirstName + " " + this.LastName;
            return (name.CompareTo(nameC));
        }
    }
}