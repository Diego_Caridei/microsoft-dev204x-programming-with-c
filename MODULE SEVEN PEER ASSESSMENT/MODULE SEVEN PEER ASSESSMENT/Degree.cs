﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_SEVEN_PEER_ASSESSMENT
{
    class Degree
    {
        private string degreeName;
        private int totalCredits;
        private Course course;

        internal Course Course
        {
            get { return course; }
            set { course = value; }
        } 


        public int TotalCredits
        {
            get { return totalCredits; }
            set { totalCredits = value; }
        }
        public string DegreeName
        {
            get { return degreeName; }
            set { degreeName = value; }
        } 
       

        public Degree(string name, int credits, Course course)
        {
            this.DegreeName = name;
            this.TotalCredits = credits;
            this.Course = course;
        }
    }
}