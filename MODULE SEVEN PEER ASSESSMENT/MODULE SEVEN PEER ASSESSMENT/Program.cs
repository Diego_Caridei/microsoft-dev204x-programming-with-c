﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_SEVEN_PEER_ASSESSMENT
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("Diego", "Caridei", new DateTime(1990, 10, 22), "via tortora", "Napoli", "Italia", "80100", "Italy");
            Student student2 = new Student("Fabio", "Nisci", new DateTime(1980, 10, 22), "via carmelo", "Napoli", "Italia", "80100", "Italy");
            Student student3 = new Student("Carmine", "Caronte", new DateTime(1990, 10, 22), "via tassp", "Milano", "Italia", "80100", "Italy");

            ArrayList students = new ArrayList();
            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            foreach (Student s in students)
            {
                for (int i = 0; i < 5; i++)
                {
                    double grade = i + 5.53;
                    s.addGrade(grade);
                }
            }

            Course course = new Course("Programming with C#", 15);
            course.addStudent(student1);
            course.addStudent(student2);
            course.addStudent(student3);

            foreach (Student s in students)
            {
                s.popGrade();
                s.addGrade(10);
            }

            foreach (Student s in students)
            {
                double grade5 = s.popGrade();
                double grade4 = s.popGrade();
                s.popGrade();
                s.addGrade(0);
                s.addGrade(grade4);
                s.addGrade(grade5);
            }

            course.listStudents();

            Console.ReadKey();

        }
    }
}
