﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_SEVEN_PEER_ASSESSMENT
{
    class UProgram
    {
        private string programName;
        private string departmentHead;
        private Degree degree;

        internal Degree Degree
        {
            get { return degree; }
            set { degree = value; }
        }

       
        public string DepartmentHead
        {
            get { return departmentHead; }
            set { departmentHead = value; }
        }
      
        public string ProgramName
        {
            get { return programName; }
            set { programName = value; }
        }
  

        public UProgram(string name, string departmentHead, Degree degree)
        {
            this.ProgramName = name;
            this.DepartmentHead = departmentHead;
            this.Degree = degree;
        }
    }
}
