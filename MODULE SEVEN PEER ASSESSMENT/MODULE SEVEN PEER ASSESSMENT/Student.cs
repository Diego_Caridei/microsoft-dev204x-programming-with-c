﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_SEVEN_PEER_ASSESSMENT
{
    class Student : Person, IComparable
    {
        private static int studentsCount = 0;
        private Stack<double> grades;

        public Student(string name, string lastname, DateTime birthDate, string addressLine, string city, string state, string zip, string country){
            this.FirstName = name;
            this.LastName = lastname;
            this.BirthDate = birthDate;
            this.AddressLine = addressLine;
            this.City = city;
            this.State=state;
            this.Zip=zip;
            this.Country = country;
            studentsCount++;

        }


           public void TakeTest()
        {
            Console.WriteLine("The student {0} is taking the test.", this.FirstName + " " + this.LastName);
        }

        public void addGrade(double grade)
        {
            grades.Push(grade);
        }

        public double popGrade()
        {
            return grades.Pop();
        }

        public void showGrades()
        {
            string grades = "";
            foreach (double grade in this.grades)
            {
                grades += grade.ToString() + " ";
            }
            Console.WriteLine("Grades: {0}",grades);
        }

        public int StudentsInSchool()
        {
            return studentsCount;
        }

        public int CompareTo(object obj)
        {
            Student studentC = (Student)obj;
            string nameC = studentC.FirstName + " " + studentC.LastName;
            string name = this.FirstName + " " + this.LastName;
            return (name.CompareTo(nameC));
        }
    }
}
    

