﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Three_Assignment
{
    class Program
    {


        static void GetStudentInformation()
        {
            Console.WriteLine("Enter the student's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the student's last name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the student's birthday name: ");
            DateTime birthday = ValidateBirthday(Console.ReadLine());
            PrintStudentDetails(firstName, lastName, birthday);

        }

        static void PrintStudentDetails(string first, string last, DateTime birthday)
        {
            Console.WriteLine("The student  {0} {1} was born on: {2}", first, last, birthday);
        }




        static void GetTeacherInformation()
        {
            Console.WriteLine("Enter the teacher's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the teacher's last name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the teacher's birthday name: ");
            DateTime birthday = ValidateBirthday(Console.ReadLine());


            PrintTeacherDetails(firstName, lastName, birthday);
        }

        static void PrintTeacherDetails(string first, string last, DateTime birthday)
        {
            Console.WriteLine("The teacher {0} {1} was born on: {2}.\n", first, last, birthday);
        }

        static void GetDegreeInformation()
        {
            Console.WriteLine("Enter the name of the degree: ");
            string degreeName = Console.ReadLine();
            Console.WriteLine("Enter the school: ");
            string school = Console.ReadLine();
            Console.WriteLine("Enter the total number of credits: ");
            int totalCredits = Convert.ToInt32(Console.ReadLine());
            PrintDegreeDetails(degreeName, school, totalCredits);
        }


        static void PrintDegreeDetails(string name, string school, int credits)
        {
            Console.WriteLine("The {0} is imparted at {1}. Credits: {2}.\n", name, school, credits);
        }

        static void GetProgramInformation()
        {
            Console.WriteLine("Enter the name of the program: ");
            string programName = Console.ReadLine();
            Console.WriteLine("Enter the degrees offered in the program: ");
            string degreesOffered = Console.ReadLine();
            Console.WriteLine("Enter the name of the department head: ");
            string departmentHead = Console.ReadLine();
            PrintProgramDetails(programName, degreesOffered, departmentHead);
        }


        static void PrintProgramDetails(string programName, string degreesOffered, string departmentHead)
        {
            Console.WriteLine("The {0} offer {1} degrees. Department head: {2}.\n", programName, degreesOffered, departmentHead);
        }

        static void GetCourseInformation()
        {
            Console.WriteLine("Enter the name of the course: ");
            string courseName = Console.ReadLine();
            Console.WriteLine("Enter the field of study: ");
            string studyField = Console.ReadLine();
            Console.WriteLine("Enter the course credits: ");
            string courseCredits = Console.ReadLine();
            PrintCourseDetails(courseName, studyField, courseCredits);

        }

        static void PrintCourseDetails(string courseName, string studyField, string courseCredits)
        {
            Console.WriteLine("Course: {0}. Field of study: {1}. Credits: {2}.\n", courseName, studyField, courseCredits);
        }



        static DateTime ValidateBirthday(String date)
        {
            try
            {
                DateTime birthday = DateTime.Parse(date);

                if (birthday.AddYears(18).CompareTo(DateTime.Today) > 0)
                {
                    Console.WriteLine("The student's age is less than 18.");
                }

                return birthday;
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid date format.");
                return DateTime.Today;
            }
        }


        static void Main(string[] args)
        {
            GetStudentInformation();
            GetTeacherInformation();
            GetDegreeInformation();
            GetProgramInformation();
            GetCourseInformation();
        }
    }
}
