﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Eight_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("Diego", "Pank", new DateTime(1990, 10, 22), "Diego@mail.com");
            Student student2 = new Student("Pasquale", "Torelli", new DateTime(1950, 03, 12), "Pasquale@mail.com");
            Student student3 = new Student("Roberto", "Stanco", new DateTime(1985, 04, 03), "Roberto@mail.com");
            List<Student> students = new List<Student>();
            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            foreach (Student s in students)
            {
                for (int i = 0; i < 5; i++)
                {
                    double grade = i + 5;
                    s.addGrade(grade);
                }
            }
            Course course = new Course("Programming with C#", 15);
            course.addStudent(student1);
            course.addStudent(student2);
            course.addStudent(student3);
            course.listStudents();
            Console.ReadKey();

        }
    }
}
