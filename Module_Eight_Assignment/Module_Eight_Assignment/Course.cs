﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Eight_Assignment
{
    class Course
    {
        private const int maxStudents = 3;
        private const int maxTeachers = 3;
        private string courseName;
        private int courseCredits;
        private List<Student> students;
        private List<Teacher> teachers;

        public Course(string name, int credits)
        {
            this.CourseName = name;
            this.CourseCredits = credits;
            this.students = new List<Student>(maxStudents);
            this.teachers = new List<Teacher>(maxTeachers);
        }


        public void addStudent(Student student)
        {
            int n = this.students.Count;
            if (n < maxStudents)
            {
                this.students.Add(student);
            }
            else
            {
                Console.WriteLine("Array of students is full.");
            }
        }

        public int getStudentsNumber()
        {
            return this.students.Count;
        }

        public void addTeacher(Teacher teacher)
        {
            int n = this.teachers.Count;
            if (n < maxTeachers)
            {
                this.teachers.Add(teacher);
            }
            else
            {
                Console.WriteLine("Array of teachers is full.");
            }
        }

        public int getTeacherNumber()
        {
            return this.teachers.Count;
        }

        public void listStudents()
        {
            this.students.Sort();

            foreach (Student s in this.students)
            {
                Console.WriteLine("{0} {1}", s.FirstName, s.LastName);
                s.showGrades();
                Console.WriteLine("\n");
            }
        }

        public int CourseCredits
        {
            get { return courseCredits; }
            set { courseCredits = value; }
        }

        public string CourseName
        {
            get { return courseName; }
            set { courseName = value; }
        }
    }
}
