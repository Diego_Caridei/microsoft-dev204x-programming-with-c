﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Eight_Assignment
{
    class Person
    {
        private string firstName;
        private string lastName;
        private DateTime birthDate;
        private string email;

      


        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
    }
}
