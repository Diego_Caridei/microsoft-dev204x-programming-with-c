﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module3Methods
{
    class Program
    {


        static  void PrintSomething()
        {
            Console.WriteLine("Hello world");
            
        }

        static int Sum(int first, int second)
        {
            return first + second;
        }

        static void ReturnMultiOut(out int i, out string s)
        {
            i = 25;
            s = "Using out";
        }

        static void ReturnMultiRef(ref int j, ref string k)
        {
            j = 25;
            k = "Using out";
        }


        static double SafeDivision(double x, double y)
        {
            if (y == 0)
            {
                throw new System.DivideByZeroException();
               
            }
            return x / y;
        }

        static void Main(string[] args)
        {
            PrintSomething();

            int first = 10;
            int second = 2;
            string sValue;

            int result = Sum(first, second);
            Console.WriteLine("The sum of {0} an {1} is: {2}", first, second, result);

            /*
            ReturnMultiOut(out first, out sValue);
            Console.WriteLine("{0}, {1}", first.ToString, sValue);
            */


            double a = 98, b = 2;
            double risultato = 0.0;

            try
            {
                risultato = SafeDivision(a, b);
                Console.WriteLine("Risultato {0}", risultato);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("Attempted divide by zero");
            }

            Console.ReadKey();
            
            
        }
    }
}
