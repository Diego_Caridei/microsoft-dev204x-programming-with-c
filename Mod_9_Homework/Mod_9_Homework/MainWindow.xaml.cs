﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_9_Homework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static int iteration = -1;
        private List<Student> studentCollections;

        void add(string first, string last, string city)
        {

            Student student = new Student();
            student.FirstName = first;
            student.LastName = last;
            student.City = city;
            studentCollections.Add(student);
            
        }

        void clearTexts()
        {
            txtFirstName.Clear();
            txtLastName.Clear();
            txtCity.Clear();
        }

        void next()
        {
            if (iteration + 1 < studentCollections.Count)
            {
                iteration++;
                txtFirstName.Text = studentCollections[iteration].FirstName;
                txtLastName.Text = studentCollections[iteration].LastName;
                txtCity.Text = studentCollections[iteration].City;
            }

            if (iteration + 1 == studentCollections.Count) iteration = -1;

        }

        void previous()
        {

            if (iteration < 0) iteration = studentCollections.Count - 1;
            if (studentCollections.Count == 1) iteration = 1;
            else if (iteration == 0) iteration = studentCollections.Count;
            iteration--;

            txtFirstName.Text = studentCollections[iteration].FirstName;
            txtLastName.Text = studentCollections[iteration].LastName;
            txtCity.Text = studentCollections[iteration].City;

        }


        public MainWindow()
        {
            InitializeComponent();
            studentCollections = new List<Student>();
        }

        private void btnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            bool is_firstName_Empty = string.IsNullOrWhiteSpace(txtFirstName.Text);
            bool is_lastName_Empty = string.IsNullOrWhiteSpace(txtLastName.Text);
            bool is_city_Empty = string.IsNullOrWhiteSpace(txtCity.Text);

            if (!is_firstName_Empty && !is_lastName_Empty && !is_city_Empty)
            {
                add(txtFirstName.Text, txtLastName.Text, txtCity.Text);
                clearTexts();
            }

            else
            {
                MessageBox.Show("One or more than One Text Area is Either empty or has white space");
            }

        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            next();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            previous();
        }
    }
}