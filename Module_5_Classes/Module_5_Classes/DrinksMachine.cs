﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_5_Classes
{
    class DrinksMachine
    {
        public DrinksMachine(string loc, string make, string model)
        {
            this.Location = loc;
            this.Make = make;
            this.Model = model;
        }
        public DrinksMachine()
        {

        }

        private string _location;

        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }


        private string _make;

        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }
        private string _model;

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }



        public void MakeCapuccino()
        {
            Console.WriteLine("Capuccino is made");
        }

        public void MakeEspresso()
        {
            Console.WriteLine("");

        }
    }
}
