﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_5_Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            DrinksMachine myMachine = new DrinksMachine("Kitchen","Brand","DM100");

            /*
            myMachine.Location = "Kitchen";
            myMachine.Model = "DM1000";
            */
            Console.WriteLine(myMachine.Location); 
            Console.WriteLine(myMachine.Make);
            Console.WriteLine(myMachine.Model);
            myMachine.MakeCapuccino();


            //Anon clas
            var anon = new { Name = "Diego", Surname = "Caridei", Age = 110 };
            Console.WriteLine(anon.Name + " " + anon.Surname+" "+ anon.Age);
            Console.ReadKey();
        }
    }
}
