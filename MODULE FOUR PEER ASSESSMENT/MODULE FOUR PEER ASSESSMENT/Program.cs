﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODULE_FOUR_PEER_ASSESSMENT
{
    class Program
    {
        public struct Student
        {
            public Student(string studentFirstName, string studentLastName, string studentCity, string studentZipOrPostal)
            {
                this.studentFirstName = studentFirstName;
                this.studentLastName = studentLastName;
                this.studentCity = studentCity;
                this.studentZipOrPostal = studentZipOrPostal;
            }
            public string studentFirstName;
            public string studentLastName;
            public string studentCity;
            public string studentZipOrPostal;
        }

        public struct Teacher
        {
            public Teacher(string teacherFirstName, string teacherLastName, string teacherCity, string teacherZipOrPostal)
            {
                this.teacherFirstName = teacherFirstName;
                this.teacherLastName = teacherLastName;
                this.teacherCity = teacherCity;
                this.teacherZipOrPostal = teacherZipOrPostal;
            }
            public string teacherFirstName;
            public string teacherLastName;
            public string teacherCity;
            public string teacherZipOrPostal;
        }

        public struct CourseProgram
        {
            public CourseProgram(string programName, string programDepartmentHead, double programDegrees)
            {
                this.programName = programName;
                this.programDepartmentHead = programDepartmentHead;
                this.programDegrees = programDegrees;
            }
            public string programName;
            public string programDepartmentHead;
            public double programDegrees;
        }

        public struct Course
        {
            public Course(string courseName, double courseCredits, int courseDuration)
            {
                this.courseName = courseName;
                this.courseCredits = courseCredits;
                this.courseDuration = courseDuration;
            }
            public string courseName;
            public double courseCredits;
            public int courseDuration;
        }

        static void Main(string[] args)
        {
            Student[] students = new Student[5];
            students[0] = new Student("Diego", "Caridei", "Napoli", "80100");
            students[1] = new Student("Fabio", "Nisci", "Milano", "20040");
            students[2] = new Student("Nicola", "Cappo", "Bari", "12002");
            students[3] = new Student("Sara", "Panico", "Firenze", "10020");
            students[4] = new Student("Simona", "Grano", "Torino", "1020");

            foreach (Student stud in students)
            {
                System.Console.WriteLine("{0} {1} from {2}, {3} ", stud.studentFirstName, stud.studentLastName, stud.studentCity, stud.studentZipOrPostal);
            }
            Console.ReadKey();
        }
    }
}