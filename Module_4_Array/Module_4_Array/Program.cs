﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_4_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] intArray = new int[10];
            intArray[0] = 10;
            intArray[1] = 20;

            int [] newArray = { 1, 2, 3, 4, 5 };
            for (int i = 0; i < newArray.Length; i++)
            {
                Console.WriteLine(newArray[i]);
            }

            //Matrix 

            int[,] matrix = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    int values = matrix[i, j];
                    Console.Write(values);
                }
            }

                Console.ReadKey();
        }
    }
}
