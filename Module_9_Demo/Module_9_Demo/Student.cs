﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_9_Demo
{
    class Student
    {
        private string firstName;
        private string lastName;
        private string city;

        public string City
        {
            get { return city; }
            set { city = value; }
        }


        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }



        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
    }
}
