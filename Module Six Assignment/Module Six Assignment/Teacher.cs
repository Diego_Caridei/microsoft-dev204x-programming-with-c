﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Six_Assignment
{
    class Teacher:Person
    {
        private static int teachersCounter = 0;


        public Teacher(string name, string lastname, DateTime birthDate, string addressLine, string city, string state, string zip, string country)
        {
            this.FirstName = name;
            this.LastName = lastname;
            this.BirthDate = birthDate;
            this.AddressLine = addressLine;
            this.City = city;
            this.State=state;
            this.Zip=zip;
            this.Country = country;
            teachersCounter++;

        }

        public void GradeTest()
        {
            Console.WriteLine("The teacher {0} is grading the test.", this.FirstName + " " + this.LastName);
        }

        public void PublishQualifications()
        {
            Console.WriteLine("The teacher {0} is publishing the Qualifications.", this.FirstName + " " + this.LastName);
        }

        public int TeachersInSchool()
        {
            return teachersCounter;
        }
    }
}
