﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Six_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {

            Student student1 = new Student("Diego", "Caridei", new DateTime(1990, 10, 22), "via tortora","Napoli","Italia","80100","Italy");
            Student student2 = new Student("Fabio", "Nisci", new DateTime(1980, 10, 22), "via carmelo", "Napoli", "Italia", "80100", "Italy");
            Student student3 = new Student("Carmine", "Caronte", new DateTime(1990, 10, 22), "via tassp", "Milano", "Italia", "80100", "Italy");
            Course course = new Course("Programming with C#", 15);

            course.addStudent(student1);
            course.addStudent(student2);
            course.addStudent(student3);

            Teacher teacher = new Teacher("Giulio", "Giunta", new DateTime(1960, 10, 22), "via tortora", "Napoli", "Italia", "80100", "Italy");

            course.addTeacher(teacher);

            Degree degree = new Degree("Bachelor of Science", 250, null);

            degree.Course = course;

            UProgram uProgram = new UProgram("Information Technology", "Gustavo Fring", null);

            uProgram.Degree = degree;

            Console.WriteLine("The {0} program contains the {1} degree\n", uProgram.ProgramName, uProgram.Degree.DegreeName);
            Console.WriteLine("The {0} degree contains the course {1}\n", uProgram.Degree.DegreeName, uProgram.Degree.Course.CourseName);
            Console.WriteLine("The {0} course contains {1} student(s)\n", uProgram.Degree.Course.CourseName, uProgram.Degree.Course.StudentsNumber);

            Person person1 = new Person();
            Student student4 = new Student("Dino", "Capasso", new DateTime(1990, 10, 22), "via tortora", "Napoli", "Italia", "80100", "Italy");
            person1 = student4;
            Console.WriteLine("{0}, {1}, {2}", person1.FirstName + " " + person1.LastName, person1.BirthDate, person1.AddressLine);
            Console.ReadKey();
        }
    }
}