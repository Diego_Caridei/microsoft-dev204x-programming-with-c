﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Six_Assignment
{
    class Student:Person
    {
        private static int studentsCounter = 0;


        public Student(string name, string lastname, DateTime birthDate, string addressLine, string city, string state, string zip, string country)
        {
            this.FirstName = name;
            this.LastName = lastname;
            this.BirthDate = birthDate;
            this.AddressLine = addressLine;
            this.City = city;
            this.State=state;
            this.Zip=zip;
            this.Country = country;
            studentsCounter++;

        }

        public void TakeTest()
        {
            Console.WriteLine("The student {0} is taking the test.", this.FirstName + " " + this.LastName);
        }

        public void CheckQualifications()
        {
            Console.WriteLine("The student {0} is checking the Qualifications.", this.FirstName + " " + this.LastName);
        }

        public int StudentsInSchool()
        {
            return studentsCounter;
        }

    }
}
