﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_Six_Assignment
{
    class UProgram
    {
        public string ProgramName { get; set; }
        public string DepartmentHead { get; set; }
        public Degree Degree { get; set; }

        // Constructor.
        public UProgram(string pName, string departmentH, Degree degree)
        {
            this.ProgramName = pName;
            this.DepartmentHead = departmentH;
            this.Degree = degree;
        }
    }
}
